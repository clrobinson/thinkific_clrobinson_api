# README

This Ruby on Rails API has been deployed to [https://frozen-meadow-72613.herokuapp.com](https://frozen-meadow-72613.herokuapp.com). The root (`/`) directory is a simple HTML page. The API routes can be accessed via `cURL`.

## Assumptions

* The `cURL` examples all used `https`; I chose Heroku for deployment because `https` is enabled by default.
* The user story claimed that the code to be accessing the API is JavaScript, but the pseudocode didn't follow usual JavaScript style guides, namely camelCase. Given this, the JSON response for the integer-related endpoints followed the style of the more ruby-ish pseudocode, and returned the integer as a value of `current_int`.
* Integer incrementing and assignment were done using methods that intentionally bypassed User model validations; the model validations done for registration don't need to run every time a user's integer is updated.
* I omitted `type` or `id` of the JSON API spec in the responses, as these didn't seem appropriate for the payload: an integer or an API key.
* I did not use a dedicated view to represent the JSON payloads, because the payloads were very simple. For such a simple service, rendering JSON in the controller seemed more maintainable and readable.

## Endpoints

All of the example routes in the challenge description PDF have been accounted for:

**Fetch the next integer (requires API key):**

`GET https://frozen-meadow-72613.herokuapp.com/v1/next`

**Fetch the current integer (requires API key):**

`GET https://frozen-meadow-72613.herokuapp.com/v1/current`

**Change the integer to a non-negative value (requires API key):**

`PUT https://frozen-meadow-72613.herokuapp.com/v1/current`

**Register a new user:**

`POST https://frozen-meadow-72613.herokuapp.com/v1/users`

**Retrieve an existing user's API key:**

`POST https://frozen-meadow-72613.herokuapp.com/v1/users/authenticate`

## Installation and Running the Code

### Getting Started

#### Install Postgres

First, install Postgres.

A good Mac guide is [available here](https://www.codementor.io/engineerapart/getting-started-with-postgresql-on-mac-osx-are8jcopb) that uses `homebrew`.

A Windows tutorial is [available here](http://www.postgresqltutorial.com/install-postgresql/), though I can't personally vouch for it (I use a Mac).

#### Ruby Version Manager

I strongly recommend [installing](https://rvm.io/rvm/install) and using `rvm` (Ruby Version Manager) before continuing with the Rails app set-up.

Once `rvm` is installed, make sure you're using the correct version of Ruby. The required version is "2.5.3", which you can ensure with `rvm install 2.5.3` and use with `rvm use 2.5.3`.

Also, create a gem set for installation. You can do this with `rvm gemset create NAME_OF_YOUR_GEMSET` and `rvm gemset use NAME_OF_YOUR_GEMSET`.

Note that setting `rvm use 2.5.3` and `rvm gemset use NAME_OF_YOUR_GEMSET` should be linked to a Mac's command line (terminal) window. With a new window, you'll have to run these two commands again.

#### Clone the Repo

Next, clone this repo to your machine. Bitbucket has [a pretty comprehensive help article](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html).

#### Bundle Install

Next, `cd` into the directory where you cloned the repo. If you don't have the `bundler` gem installed, you can install it with `gem install bundler`.

Once that's done, run `bundle install`.

#### Set Up the Database

Run `rails db:create` and `rails db:migrate` to set up the Postgres database for this Rails app.

### Running the Test Suite

Tests were written in [RSpec](http://rspec.info/).

To run the test suite, run `bundle exec rspec`.

You can also run `bundle exec rspec -fd` to see the full structure of the test suite as you run the tests.

### Running the App

To run the app, run `rails server` or just `rails s`. By default, the Rails API should be available at `http://localhost:3000/`.

To stop the server, press "control-C".