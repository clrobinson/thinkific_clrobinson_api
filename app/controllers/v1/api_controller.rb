class V1::ApiController < ApplicationController
  skip_before_action :verify_authenticity_token

  before_action :assign_jsonapi_content_type

  def current_user
    return @current_user if @current_user
    return unless auth_token_present?
    token = decoded_auth_token
    return unless token
    user = User.find_by_id(token["user_id"])
    if user
      @current_user ||= user
    else
      nil
    end
  end

  def logged_in?
    !!current_user
  end

  def authenticate_action
    render(json: { errors: ["Unauthorized"] }, status: 401) unless logged_in?
  end

  private

  def auth_token_present?
    !!request.env.fetch("HTTP_AUTHORIZATION", "").scan(/Bearer/).flatten.first
  end

  def decoded_auth_token
    JwtAuth.decode(auth_token)
  end

  def auth_token
    request.env["HTTP_AUTHORIZATION"].scan(/Bearer (.*)$/).flatten.first
  end

  def assign_jsonapi_content_type
    self.content_type = "application/vnd.api+json"
  end
end
