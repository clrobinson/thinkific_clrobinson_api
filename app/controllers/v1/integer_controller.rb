class V1::IntegerController < V1::ApiController
  before_action :authenticate_action

  def next_int
    current_user.increment!(:current_int)
    render current_int_json
  end

  def current_int
    render current_int_json
  end

  def assign_int
    int_to_assign = params[:current].to_i
    if int_to_assign >= 0
      current_user.update_attributes(current_int: int_to_assign)
      render current_int_json
    else
      render json: { errors: ["Integer must be equal to or greater than zero"] }, status: 422
    end
  end

  private

  def current_int_json
    {
      json: {
        data: {
          current_int: current_user.current_int
        }
      }
    }
  end
end
