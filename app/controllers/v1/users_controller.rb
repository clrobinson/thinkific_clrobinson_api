class V1::UsersController < V1::ApiController
  def authenticate
    existing_user = User.find_by_email(user_params[:email])
    if existing_user && existing_user.authenticate(user_params[:password])
      render json: {
        data: {
          api_key: existing_user.token
        }
      }
    else
      render json: {}, status: 401
    end
  end

  def create
    new_user = User.new(user_params)
    if new_user.save
      new_user.authenticate(user_params[:password])
      render json: {
        data: {
          api_key: new_user.token
        }
      }
    else
      render json: { errors: new_user.errors.full_messages }, status: 422
    end
  end

  private

  def user_params
    params.permit(
      :email,
      :password
    )
  end
end
