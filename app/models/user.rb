require 'bcrypt'

class User < ApplicationRecord
  include BCrypt

  attr_reader :token

  before_save :downcase_email

  validates :email, presence: true, uniqueness: true
  validate :password_should_not_be_blank

  def authenticate(password)
    current_password = Password.new(password_digest)
    @token = if current_password == password
               JwtAuth.encode({ user_id: id })
             end
    !!token
  end

  def password=(new_password)
    return if new_password.blank?
    self.password_digest = Password.create(new_password)
  end

  private

  def downcase_email
    return unless email && email_changed?
    self.email = email.downcase
    true
  end

  def password_should_not_be_blank
    return true if password_digest.present?
    errors.add(:password, "can't be blank")
  end
end
