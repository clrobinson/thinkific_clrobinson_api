Rails.application.routes.draw do
  root to: 'static#root'

  namespace :v1 do
    # Users
    post 'users/authenticate' => 'users#authenticate'
    post 'users' => 'users#create'

    # Integers
    get 'next' => 'integer#next_int'
    get 'current' => 'integer#current_int'
    put 'current' => 'integer#assign_int'
  end
end
