class AddCurrentIntToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :current_int, :integer, default: 0, null: false
  end
end
