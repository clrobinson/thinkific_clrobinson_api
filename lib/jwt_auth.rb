require 'jwt'

class JwtAuth
  ALGORITHM = 'HS256'.freeze

  def self.encode(payload)
    JWT.encode(payload,
               auth_secret,
               ALGORITHM)
  end

  def self.decode(token)
    begin
      JWT.decode(token,
                 auth_secret,
                 true,
                 { algorithm: ALGORITHM }).first
    rescue JWT::VerificationError
      nil
    end
  end

  private

  def self.auth_secret
    Rails.application.credentials.hmac_secret
  end
end
