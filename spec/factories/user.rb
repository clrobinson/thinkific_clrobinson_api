FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.email }
    after(:build) do |user|
      user.password = Faker::Alphanumeric.alpha(12)
    end
  end
end
