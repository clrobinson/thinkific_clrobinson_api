require 'rails_helper'

describe JwtAuth do
  let(:test_data) { { 'test' => 'data' } }
  let(:token) { "eyJhbGciOiJIUzI1NiJ9.eyJ0ZXN0IjoiZGF0YSJ9.TviL8Rl4f_F9JcRq2JorWUU230Eysj0loq8bvxhm2cI" }

  it "should encode a JWT" do
    expect(JwtAuth.encode(test_data)).to eq(token)
  end

  it "should decode a JWT" do
    expect(JwtAuth.decode(token)).to eq(test_data)
  end
end
