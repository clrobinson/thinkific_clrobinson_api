require 'rails_helper'
require 'bcrypt'

describe User, type: :model do
  let(:user) { create(:user) }

  describe ".create" do
    it "can save successfully" do
      user.save!
      expect(User.count).to eq(1)
    end

    it "is invalid without an email" do
      user.email = nil
      user.save
      expect(user.errors.full_messages).to include("Email can't be blank")
    end

    it "is invalid without a unique email" do
      new_user = build(:user, email: user.email)
      new_user.save
      expect(new_user.errors.full_messages).to include("Email has already been taken")
    end

    it "downcases the supplied email" do
      user.email = "UsEr@EmAiL.CoM"
      user.save!
      user.reload
      expect(user.email).to eq("user@email.com")
    end

    it "is invalid without a password digest" do
      user.password_digest = nil
      user.save
      expect(user.errors.full_messages).to include("Password can't be blank")
    end
  end

  describe ".password=" do
    it "should encrypt the passed string in 'password_digest'" do
      user.password = "new_password"
      user.save
      user.reload
      expect(BCrypt::Password.new(user.password_digest)).to eq("new_password")
    end
  end

  describe ".authenticate" do
    it "should return a JWT upon success" do
      allow_any_instance_of(BCrypt::Password).to receive(:==).and_return(true)
      expect(user.authenticate("anything")).to eq(true)
    end

    it "should return 'false' upon failure" do
      allow_any_instance_of(BCrypt::Password).to receive(:==).and_return(false)
      expect(user.authenticate("anything")).to eq(false)
    end
  end

  describe ".token" do
    it "should be a valid JWT auth token after a successful authentication" do
      allow_any_instance_of(BCrypt::Password).to receive(:==).and_return(true)
      allow(user).to receive(:id).and_return(1)
      user.authenticate("anything")
      token = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._HlNWHIBG6lsmw6EdhNO7kC164D_9HYwdyncm6KwQI0"
      expect(user.token).to eq(token)
    end

    it "should be 'nil' after a failed authentication" do
      allow_any_instance_of(BCrypt::Password).to receive(:==).and_return(false)
      user.authenticate("anything")
      expect(user.token).to eq(nil)
    end
  end
end
