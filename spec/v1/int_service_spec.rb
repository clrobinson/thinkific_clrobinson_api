require 'rails_helper'

RSpec.describe "Integer Service", type: :request do
  before do
    @user = build(:user)
    @user.password = "userPASS1"
    @user.save
    @user.authenticate("userPASS1")
    @token = @user.token
  end

  describe "GET /v1/next" do
    it "verifies authorization" do
      get "/v1/next", headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token + '0'}" }
      expect(response.status).to eq(401)
      response_json = JSON.parse(response.body)
      expect(response_json.keys).to include("errors")
      expect(response_json["errors"]).to include("Unauthorized")
    end

    it "returns the user's next integer" do
      current_int = @user.current_int
      get "/v1/next", headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token}" }
      response_json = JSON.parse(response.body)
      expect(response_json["data"].keys).to include("current_int")
      expect(response_json["data"]["current_int"]).to eq(current_int + 1)
    end

    it "increments the user's current integer by one" do
      current_int = @user.current_int
      get "/v1/next", headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token}" }
      @user.reload
      expect(@user.current_int).to eq(current_int + 1)
    end
  end

  describe "GET /v1/current" do
    it "verifies authorization" do
      get "/v1/current", headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token + '0'}" }
      expect(response.status).to eq(401)
      response_json = JSON.parse(response.body)
      expect(response_json.keys).to include("errors")
      expect(response_json["errors"]).to include("Unauthorized")
    end

    it "returns the user's current integer" do
      current_int = @user.current_int
      get "/v1/current", headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token}" }
      response_json = JSON.parse(response.body)
      expect(response_json["data"].keys).to include("current_int")
      expect(response_json["data"]["current_int"]).to eq(current_int)
    end

    it "does NOT increment the user's integer by one" do
      current_int = @user.current_int
      get "/v1/current", headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token}" }
      @user.reload
      expect(@user.current_int).to eq(current_int)
    end
  end

  describe "PUT /v1/current" do
    it "verifies authorization" do
      put "/v1/current", params: { current: 25 }, headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token + '0'}" }
      expect(response.status).to eq(401)
      response_json = JSON.parse(response.body)
      expect(response_json.keys).to include("errors")
      expect(response_json["errors"]).to include("Unauthorized")
    end

    it "checks that the submitted integer is non-negative" do
      put "/v1/current", params: { current: -1 }, headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token}" }
      expect(response.status).to eq(422)
      response_json = JSON.parse(response.body)
      expect(response_json.keys).to include("errors")
      expect(response_json["errors"]).to include("Integer must be equal to or greater than zero")
    end

    it "assigns the user's current integer to the submitted value" do
      current_int = @user.current_int
      put "/v1/current", params: { current: 25 }, headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token}" }
      @user.reload
      expect(@user.current_int).to eq(25)
    end

    it "returns the newly assigned integer value" do
      current_int = @user.current_int
      put "/v1/current", params: { current: 25 }, headers: { "HTTP_AUTHORIZATION" => "Bearer #{@token}" }
      response_json = JSON.parse(response.body)
      expect(response_json["data"].keys).to include("current_int")
      expect(response_json["data"]["current_int"]).to eq(25)
    end
  end
end
