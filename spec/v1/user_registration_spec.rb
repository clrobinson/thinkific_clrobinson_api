require 'rails_helper'

RSpec.describe "User Registration", type: :request do
  let(:existing_user) { create(:user) }

  describe "POST /v1/users" do
    it "creates a new user" do
      post "/v1/users", params: { email: "sample@email.com", password: "samplePASS1" }
      expect(User.count).to eq(1)
      created_user = User.first
      expect(created_user.email).to eq("sample@email.com")
    end

    it "returns an API key" do
      post "/v1/users", params: { email: "sample@email.com", password: "samplePASS1" }
      json_response = JSON.parse(response.body)
      expect(json_response["data"].keys).to include("api_key")
      created_user = User.first
      created_user.authenticate("samplePASS1")
      token = created_user.token
      expect(json_response["data"]["api_key"]).to eq(token)
    end

    it "returns errors" do
      duplicate_email = existing_user.email
      post "/v1/users", params: { email: duplicate_email, password: "samplePASS1" }
      json_response = JSON.parse(response.body)
      expect(json_response.keys).to include("errors")
      expect(json_response["errors"]).to include("Email has already been taken")
    end
  end

  describe "POST /v1/authenticate" do
    before do
      existing_user.password = "existingPASS1"
      existing_user.save
    end

    it "returns an API key" do
      post "/v1/users/authenticate", params: { email: existing_user.email, password: "existingPASS1" }
      json_response = JSON.parse(response.body)
      expect(json_response["data"].keys).to include("api_key")
      existing_user.reload
      existing_user.authenticate("existingPASS1")
      token = existing_user.token
      expect(json_response["data"]["api_key"]).to eq(token)
    end

    it "returns 401 if the user does not exist" do
      post "/v1/users/authenticate", params: { email: existing_user.email + "a", password: "existingPASS1" }
      expect(response.status).to eq(401)
    end

    it "returns 401 if the password is incorrect" do
      post "/v1/users/authenticate", params: { email: existing_user.email, password: "badpassword" }
      expect(response.status).to eq(401)
    end
  end
end
